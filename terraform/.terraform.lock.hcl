# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/vsphere" {
  version = "2.2.0"
  hashes = [
    "h1:ysSuGN3jMzH0IRWyBDrbt6cTpxZZcd8QHGA7Re7B1OM=",
  ]
}
